# Unidad de Trabajo 04. Práctica a Entregar

Realizar un pequeño programa en Java que permita enviar un correo electrónico utilizando una cuenta de correo de __Gmail__. Para ello haremos uso de las __JavaMail API__.

Dependencias necesarias:

```bash
<!-- https://mvnrepository.com/artifact/javax.mail/mail -->
<dependency>
    <groupId>javax.mail</groupId>
    <artifactId>mail</artifactId>
    <version>1.4.5</version>
</dependency>
```

Detalles de la configuración de la cuenta de correo Gmail: [http://mail.google.com/support/bin/answer.py?hl=en&answer=13287](http://mail.google.com/support/bin/answer.py?hl=en&answer=13287)